package controller;

import model.Consulta;
import model.Funcionario;
import model.Medico;
import model.Paciente;

public class Fachada <T>{
    private final Funcionario funcionario;
    private final Medico medico;
    private final Paciente paciente;
    private final Consulta consulta;
    
    
    Fachada(){
        this.funcionario = new Funcionario();
        this.medico = new Medico();
        this.paciente = new Paciente();
        this.consulta = new Consulta();

    }


	public Funcionario getFuncionario() {
		return funcionario;
	}


	public Medico getMedico() {
		return medico;
	}


	public Paciente getPaciente() {
		return paciente;
	}


	public Consulta getConsulta() {
		return consulta;
	}
    
    
	
}
