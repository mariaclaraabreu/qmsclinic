package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Consultorio;
import model.Paciente;

public final class PacienteDao implements DaoGenerico<Paciente> {

	private final ArrayList<Paciente> dados = new ArrayList<>();

	@Override
	public void cadastrar(Paciente objeto) throws Exception {
		String sql = "insert into paciente(cpf, nome, endereco, telefone, email)values(?,?,?,?,?)";
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);

			stmt.setString(1, objeto.getCpf());
			stmt.setString(2, objeto.getNome());
			stmt.setString(3, objeto.getEndereco());
			stmt.setString(4, objeto.getTelefone());
			stmt.setString(5, objeto.getEmail());
		
			dados.add(objeto);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			throw new SQLException("Paciente n�o foi cadastrado" + e);
		} finally {
			Conexao.getConnection().close();

		}
		
	}

	@Override
	public Paciente buscar(Paciente objeto) throws Exception {
		 String sql = "select * from paciente where cpf=?";
	        try {
	            PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
	            stmt.setString(1, objeto.getCpf());
	            ResultSet rs = stmt.executeQuery();
	            if(rs.next())
	                objeto = new Paciente(rs.getString("cpf"), rs.getString("nome"), rs.getString("endereco"),
	            			rs.getString("telefone"), rs.getString("email"));
	            dados.clear();
	            dados.add(objeto);
	            stmt.close();
	            rs.close();
	        } catch (SQLException ex) {
	            throw new SQLException("Error ao buscar paciente"+ex);
	        }finally{
	            Conexao.getConnection().close();
	        }
	        return objeto;
	}

	@Override
	public ArrayList<Paciente> buscar() throws Exception {
		 String sql = "select * from paciente";
	        ArrayList<Paciente> dados = new ArrayList<>();
	        try {
	            PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
	            ResultSet rs = stmt.executeQuery();
	            if(rs.next())
	                dados.add(new Paciente(rs.getString("cpf"), rs.getString("nome"), rs.getString("endereco"),rs.getString("telefone"), rs.getString("email")));
	            dados.clear();
	            dados.addAll(dados);
	            stmt.close();
	            rs.close();
	        } catch (SQLException ex) {
	            throw new SQLException("Erro ao buscar paciente"+ex);
	        }finally{
	            Conexao.getConnection().close();
	        }
	        return dados;
	}

	@Override
	public ArrayList<Paciente> buscar(String coluna, String valor) throws Exception {
		  String sql = "select * from paciente where .? like %?%"; // verfiicar like
	        ArrayList<Paciente> dados = new ArrayList<>();
	        try {
	            PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
	            stmt.setString(1, coluna);
	            stmt.setString(2, valor);
	            ResultSet rs = stmt.executeQuery();
	            if(rs.next())
	                dados.add(new Paciente(rs.getString("cpf"), rs.getString("nome"), rs.getString("endereco"),rs.getString("telefone"), rs.getString("email")));
	            dados.clear();
	            dados.addAll(dados);
	            stmt.close();
	            rs.close();
	        } catch (SQLException ex) {
	            throw new SQLException("Error ao buscar paciente"+ex);
	        }finally{
	            Conexao.getConnection().close();
	        }
	        return dados;
	}

	@Override
	public void alterar(Paciente objeto) throws Exception {
		String sql = "update paciente set cpf = ?, nome = ?, endereco = ?, telefone = ?, email = ? where cpf = ?";
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);

			
			stmt.setString(1, objeto.getCpf());
			stmt.setString(2, objeto.getNome());
			stmt.setString(3, objeto.getEndereco());
			stmt.setString(4, objeto.getTelefone());
			stmt.setString(5, objeto.getEmail());
			
			dados.remove(objeto);
			dados.add(objeto);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			throw new SQLException("Paciente n�o foi alterado" + e);
		} finally {
			Conexao.getConnection().close();

		}
		
	}

	@Override
	public void remover(Paciente objeto) throws Exception {
		String sql = "delete from paciente where cpf = ?";
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
			stmt.setString(1, objeto.getCpf());
			dados.remove(objeto);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException ex) {
			throw new SQLException("Paciente n�o foi removido" + ex);
		} finally {
			Conexao.getConnection().close();
		}
		
	}

	@Override
	public ArrayList<Paciente> buscarHistorico() {
		// TODO Auto-generated method stub
		return dados;
	}


	

}
