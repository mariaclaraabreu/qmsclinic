package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import exception.CpfInvalidoException;
import model.Consultorio;
//import model.Administrador;
//import model.Cpf;
import model.Funcionario;
import model.Paciente;

public final class  FuncionarioDao implements DaoGenerico <Funcionario>{
	
	
	private final ArrayList <Funcionario> dados = new ArrayList<>();

	@Override
	public void cadastrar(Funcionario objeto) throws Exception {
		String sql = "insert into funcionario(cpf, senha)values(?,?)";
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);

			stmt.setString(1, objeto.getCpf());
			stmt.setString(2, objeto.getSenha());
		
		
			dados.add(objeto);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			throw new SQLException("Funcionario n�o foi cadastrado" + e);
		} finally {
			Conexao.getConnection().close();

		}
		
	}

	@Override
	public Funcionario buscar(Funcionario objeto) throws Exception {
		 String sql = "select * from funcionario where cpf=?";
	        try {
	            PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
	            stmt.setString(1, objeto.getCpf());
	            ResultSet rs = stmt.executeQuery();
	            if(rs.next())
	                objeto = new Funcionario(rs.getString("cpf"), rs.getString("senha"));
	            dados.clear();
	            dados.add(objeto);
	            stmt.close();
	            rs.close();
	        } catch (SQLException ex) {
	            throw new SQLException("Error ao buscar funcionario"+ex);
	        }finally{
	            Conexao.getConnection().close();
	        }
	        return objeto;
	}

	@Override
	public ArrayList<Funcionario> buscar() throws Exception {
		 String sql = "select * from funcionario";
	        ArrayList<Funcionario> dados = new ArrayList<>();
	        try {
	            PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
	            ResultSet rs = stmt.executeQuery();
	            if(rs.next())
	                dados.add(new Funcionario(rs.getString("cpf"), rs.getString("senha")));
	            dados.clear();
	            dados.addAll(dados);
	            stmt.close();
	            rs.close();
	        } catch (SQLException ex) {
	            throw new SQLException("Erro ao buscar funcionario"+ex);
	        }finally{
	            Conexao.getConnection().close();
	        }
	        return dados;
	}

	@Override
	public ArrayList<Funcionario> buscar(String coluna, String valor) throws Exception {
		  String sql = "select * from funcionario where .? like %?%"; // verfiicar like
	        ArrayList<Funcionario> dados = new ArrayList<>();
	        try {
	            PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
	            stmt.setString(1, coluna);
	            stmt.setString(2, valor);
	            ResultSet rs = stmt.executeQuery();
	            if(rs.next())
	                dados.add(new Funcionario(rs.getString("cpf"), rs.getString("senha")));
	            dados.clear();
	            dados.addAll(dados);
	            stmt.close();
	            rs.close();
	        } catch (SQLException ex) {
	            throw new SQLException("Error ao buscar funcionario"+ex);
	        }finally{
	            Conexao.getConnection().close();
	        }
	        return dados;
	}

	@Override
	public void alterar(Funcionario objeto) throws Exception {
		String sql = "update funcionario set senha = ? where cpf = ?";
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);

			
			stmt.setString(1, objeto.getSenha());
			
			dados.remove(objeto);
			dados.add(objeto);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			throw new SQLException("Funcionario n�o foi alterado" + e);
		} finally {
			Conexao.getConnection().close();

		}
	}

	@Override
	public void remover(Funcionario objeto) throws Exception {
		String sql = "delete from funcionario where cpf = ?";
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
			stmt.setString(1, objeto.getCpf());
			dados.remove(objeto);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException ex) {
			throw new SQLException("Funcionario n�o foi removido" + ex);
		} finally {
			Conexao.getConnection().close();
		}
		
		
	}

	@Override
	public ArrayList<Funcionario> buscarHistorico() {

		return dados;
	}
	
	





}
