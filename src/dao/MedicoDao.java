package dao;


import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Consultorio;
import model.Funcionario;
import model.Medico;

public final class MedicoDao implements DaoGenerico <Medico>{
	
	private final ArrayList <Medico> dados = new ArrayList<>();

	@Override
	public void cadastrar(Medico objeto) throws SQLException, ClassNotFoundException  {
		
		String sql = "insert into medico(crm, senha, nome, especialidade)values(?,?,?,?)";
		try {
			
			
			PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);

			stmt.setString(1, objeto.getCrm());
			stmt.setString(2, objeto.getSenha());
			stmt.setString(3, objeto.getNome());
			stmt.setString(4, objeto.getEspecialidade());

			
			dados.add(objeto);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			throw new SQLException("M�dico n�o foi cadastrado" + e);
		} finally {
			Conexao.getConnection().close();
			
		}
		
	}
	
	

	@Override
	public Medico buscar(Medico objeto) throws SQLException, ClassNotFoundException {
		 String sql = "select * from medico where crm=?";
	        try {
	            PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
	            stmt.setString(1, objeto.getCrm());
	            ResultSet rs = stmt.executeQuery();
	            if(rs.next())
	                objeto = new Medico(rs.getString("crm"), rs.getString("senha"),rs.getString("nome"), rs.getString("especialidade"));
	            dados.clear();
	            dados.add(objeto);
	            stmt.close();
	            rs.close();
	        } catch (SQLException ex) {
	            throw new SQLException("Error ao buscar medico"+ex);
	        }finally{
	            Conexao.getConnection().close();
	        }
	        return objeto;
	}

	@Override
	public ArrayList<Medico> buscar() throws SQLException, ClassNotFoundException  {
		 String sql = "select * from medico";
	        ArrayList<Medico> dados = new ArrayList<>();
	        try {
	            PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
	            ResultSet rs = stmt.executeQuery();
	            if(rs.next())
	                dados.add(new Medico(rs.getString("crm"), rs.getString("senha"),rs.getString("nome"), rs.getString("especialidade")));
	            dados.clear();
	            dados.addAll(dados);
	            stmt.close();
	            rs.close();
	        } catch (SQLException ex) {
	            throw new SQLException("Erro ao buscar meidco"+ex);
	        }finally{
	            Conexao.getConnection().close();
	        }
	        return dados;
	}

	@Override
	public ArrayList<Medico> buscar(String coluna, String valor) throws SQLException, ClassNotFoundException  {
		  String sql = "select * from medico where .? like %?%"; // verfiicar like
	        ArrayList<Medico> dados = new ArrayList<>();
	        try {
	            PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
	            stmt.setString(1, coluna);
	            stmt.setString(2, valor);
	            ResultSet rs = stmt.executeQuery();
	            if(rs.next())
	                dados.add(new Medico(rs.getString("crm"), rs.getString("senha"),rs.getString("nome"), rs.getString("especialidade")));
	            dados.clear();
	            dados.addAll(dados);
	            stmt.close();
	            rs.close();
	        } catch (SQLException ex) {
	            throw new SQLException("Error ao buscar medico"+ex);
	        }finally{
	            Conexao.getConnection().close();
	        }
	        return dados;
	}

	@Override
	public void alterar(Medico objeto) throws SQLException, ClassNotFoundException  {
		String sql = "update medico set crm = ?, senha = ?, nome = ?, especialidade = ? where crm = ?";
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);

			
			stmt.setString(1, objeto.getCrm());
			stmt.setString(2, objeto.getSenha());
			stmt.setString(3, objeto.getNome());
			stmt.setString(4, objeto.getEspecialidade());
	
			
			dados.remove(objeto);
			dados.add(objeto);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			throw new SQLException("Medico n�o foi alterado" + e);
		} finally {
			Conexao.getConnection().close();

		}
	}

	@Override
	public void remover(Medico objeto) throws SQLException, ClassNotFoundException  {
		String sql = "delete from medico where crm = ?";
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
			stmt.setString(1, objeto.getCrm());
			dados.remove(objeto);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException ex) {
			throw new SQLException("Medico n�o foi removido" + ex);
		} finally {
			Conexao.getConnection().close();
		}
		
	}

	@Override
	public ArrayList<Medico> buscarHistorico() {
		// TODO Auto-generated method stub
		return dados;
	}




	

}
