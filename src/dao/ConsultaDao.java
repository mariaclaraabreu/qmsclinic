package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import exception.CpfInvalidoException;
import model.Consulta;
import model.Cpf;
import model.Funcionario;

public final class ConsultaDao implements DaoGenerico<Consulta> {

	private final ArrayList<Consulta> dados = new ArrayList<>();

	//antes de chamar esse metodo dever� verificar se o horario e data escolhidos batem com algum horario e data disponiveis
	//na agenda do m�dico
	
	//quando chamar esse metodo deve ser cadastrado na agenda do m�dico tb
	
	@Override
	public void cadastrar(Consulta objeto) throws Exception {
		String sql = "insert into consulta(consultorioId, medicoId, pacienteId, horarioInicio, horarioTermino, data, estado)values(?,?,?,?,?,?,?)";
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);

			stmt.setInt(1, objeto.getConsultorio().getNumero());
			stmt.setString(2, objeto.getMedico().getCrm());
			stmt.setString(3, objeto.getPaciente().getCpf());
			stmt.setString(4, objeto.getHorarioInicio());
			stmt.setString(5, objeto.getHorarioTermino());
			stmt.setString(6, objeto.getDataConsulta());
			stmt.setBoolean(7, objeto.isEstado());
			

			dados.add(objeto);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			throw new SQLException("Consulta n�o foi cadastrada" + e);
		} finally {
			Conexao.getConnection().close();

		}

	}

	@Override
	public Consulta buscar(Consulta objeto) throws Exception {
		 String sql = "select * from consulta where id=?";
	        try {
	            PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
	            stmt.setInt(1, objeto.getId());
	            ResultSet rs = stmt.executeQuery();
	            if(rs.next())
	            	//verificar atributos consultorioId?
	                objeto = new Consulta(rs.getInt("id"), rs.getInt("consultorioId"),rs.getString("medicoId"),rs.getString("pacienteId"), rs.getString("horarioInicio"), rs.getString("horarioTermino"), rs.getString("dataConsulta"), rs.getBoolean("estado"));
	            dados.clear();
	            dados.add(objeto);
	            stmt.close();
	            rs.close();
	        } catch (SQLException ex) {
	            throw new SQLException("Error ao buscar consulta"+ex);
	        }finally{
	            Conexao.getConnection().close();
	        }
	        return objeto;
	}

	@Override
	public ArrayList<Consulta> buscar() throws Exception {

		 String sql = "select * from consulta";
	        ArrayList<Consulta> dados = new ArrayList<>();
	        try {
	            PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
	            ResultSet rs = stmt.executeQuery();
	            if(rs.next())
	                dados.add(new Consulta(rs.getInt("id"), rs.getInt("consultorioId"),rs.getString("medicoId"),rs.getString("pacienteId"), rs.getString("horarioInicio"), rs.getString("horarioTermino"), rs.getString("dataConsulta"), rs.getBoolean("estado")));
	            dados.clear();
	            dados.addAll(dados);
	            stmt.close();
	            rs.close();
	        } catch (SQLException ex) {
	            throw new SQLException("Erro ao buscar consulta"+ex);
	        }finally{
	            Conexao.getConnection().close();
	        }
	        return dados;

	}

	@Override
	public ArrayList<Consulta> buscar(String coluna, String valor) throws Exception {
		  String sql = "select * from consulta where .? like %?%"; // verfiicar like
	        ArrayList<Consulta> dados = new ArrayList<>();
	        try {
	            PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
	            stmt.setString(1, coluna);
	            stmt.setString(2, valor);
	            ResultSet rs = stmt.executeQuery();
	            if(rs.next())
	                dados.add(new Consulta(rs.getInt("id"), rs.getInt("consultorioId"),rs.getString("medicoId"),rs.getString("pacienteId"), rs.getString("horarioInicio"), rs.getString("horarioTermino"), rs.getString("dataConsulta"), rs.getBoolean("estado")));
	            dados.clear();
	            dados.addAll(dados);
	            stmt.close();
	            rs.close();
	        } catch (SQLException ex) {
	            throw new SQLException("Error ao buscar consulta"+ex);
	        }finally{
	            Conexao.getConnection().close();
	        }
	        return dados;
	}

	@Override
	public void alterar(Consulta objeto) throws Exception {
		String sql = "update consulta set id = ?, consultorioId = ?, medicoId = ?, pacienteId = ?, horarioInicio = ?, horarioTermino = ?, dataConsulta = ?, estado = ?  where id = ?";
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);

			
			stmt.setInt(1, objeto.getConsultorio().getNumero());
			stmt.setString(2, objeto.getMedico().getCrm());
			stmt.setString(3, objeto.getPaciente().getCpf());
			stmt.setString(5, objeto.getHorarioInicio());
			stmt.setString(6, objeto.getHorarioTermino());
			stmt.setString(4, objeto.getDataConsulta());
			stmt.setBoolean(6, objeto.isEstado());
			
			dados.remove(objeto);
			dados.add(objeto);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			throw new SQLException("Consulta n�o foi alterada" + e);
		} finally {
			Conexao.getConnection().close();

		}

	}

	@Override
	public void remover(Consulta objeto) throws Exception {
		String sql = "delete from consulta where id = ?";
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
			stmt.setInt(1, objeto.getId());
			dados.remove(objeto);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException ex) {
			throw new SQLException("Consulta n�o foi removida" + ex);
		} finally {
			Conexao.getConnection().close();
		}
	}

	@Override
	public ArrayList<Consulta> buscarHistorico() {
		// TODO Auto-generated method stub
		return dados;
	}

	
}
