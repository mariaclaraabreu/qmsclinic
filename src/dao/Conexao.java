package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;



public class Conexao {

	
	private static Connection con;
	
	
	

	static String servidor = "jdbc:mysql://127.0.0.1:3306/clinicaMedica?useTimezone=true&serverTimezone=UTC&useSSL=false&allowPublicKeyRetrieval=true";

	static String usuario="root";
	static String senha = "18008657";
	static String driver = "com.mysql.cj.jdbc.Driver";
	
	
    public static Connection getConnection() throws ClassNotFoundException, SQLException{
        try {
            Class.forName(driver);
            con = DriverManager.getConnection(servidor, usuario, senha);
            System.out.println(con);
            return con;
        } catch (ClassNotFoundException e) {
            throw new ClassNotFoundException("Connection error",e);
        }catch (SQLException ex) {
            throw new SQLException("Connection error",ex);
        }
    }
        
    	public static Connection getCon() {
    		return con;
    	}
        
}
