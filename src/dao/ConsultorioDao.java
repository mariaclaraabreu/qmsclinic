package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import model.Consulta;
import model.Consultorio;
import model.Medico;



public class ConsultorioDao implements DaoGenerico <Consultorio>{
	private final ArrayList <Consultorio> dados = new ArrayList<>();

	@Override
	public void cadastrar(Consultorio objeto) throws Exception {
		String sql = "insert into consultorio(numero, bloco, medicoId)values(?,?,?)";
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);

			stmt.setInt(1, objeto.getNumero());
			stmt.setInt(2, objeto.getBloco());
			stmt.setString(1, objeto.getMedico().getCrm());
		
			dados.add(objeto);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			throw new SQLException("Consultorio n�o foi cadastrado" + e);
		} finally {
			Conexao.getConnection().close();

		}
		
	}

	@Override
	public Consultorio buscar(Consultorio objeto) throws Exception {
		 String sql = "select * from consultorio where numero=?";
	        try {
	            PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
	            stmt.setInt(1, objeto.getNumero());
	            ResultSet rs = stmt.executeQuery();
	            if(rs.next())
	                objeto = new Consultorio(rs.getInt("numero"), rs.getInt("bloco"),rs.getString("medicoId"));
	            dados.clear();
	            dados.add(objeto);
	            stmt.close();
	            rs.close();
	        } catch (SQLException ex) {
	            throw new SQLException("Error ao buscar consultorio"+ex);
	        }finally{
	            Conexao.getConnection().close();
	        }
	        return objeto;
	}

	@Override
	public ArrayList<Consultorio> buscar() throws Exception {
		 String sql = "select * from consultorio";
	        ArrayList<Consultorio> dados = new ArrayList<>();
	        try {
	            PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
	            ResultSet rs = stmt.executeQuery();
	            if(rs.next())
	                dados.add(new Consultorio(rs.getInt("numero"), rs.getInt("bloco"),rs.getString("medicoId")));
	            dados.clear();
	            dados.addAll(dados);
	            stmt.close();
	            rs.close();
	        } catch (SQLException ex) {
	            throw new SQLException("Erro ao buscar consultorio"+ex);
	        }finally{
	            Conexao.getConnection().close();
	        }
	        return dados;
	}

	@Override
	public ArrayList<Consultorio> buscar(String coluna, String valor) throws Exception {
		  String sql = "select * from consultorio where .? like %?%"; // verfiicar like
	        ArrayList<Consultorio> dados = new ArrayList<>();
	        try {
	            PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
	            stmt.setString(1, coluna);
	            stmt.setString(2, valor);
	            ResultSet rs = stmt.executeQuery();
	            if(rs.next())
	                dados.add(new Consultorio(rs.getInt("numero"), rs.getInt("bloco"),rs.getString("medicoId")));
	            dados.clear();
	            dados.addAll(dados);
	            stmt.close();
	            rs.close();
	        } catch (SQLException ex) {
	            throw new SQLException("Error ao buscar consultorio"+ex);
	        }finally{
	            Conexao.getConnection().close();
	        }
	        return dados;
	}

	@Override
	public void alterar(Consultorio objeto) throws Exception {
		String sql = "update consultorio set numero = ?, bloco = ?, medicoId = ? where numero = ?";
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);

			
			stmt.setInt(1, objeto.getNumero());
			stmt.setInt(2, objeto.getBloco());
			stmt.setString(1, objeto.getMedico().getCrm());
			
			dados.remove(objeto);
			dados.add(objeto);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException e) {
			throw new SQLException("Consultorio n�o foi alterado" + e);
		} finally {
			Conexao.getConnection().close();

		}
		
	}

	@Override
	public void remover(Consultorio objeto) throws Exception {
		String sql = "delete from consultorio where numero = ?";
		try {
			PreparedStatement stmt = Conexao.getConnection().prepareStatement(sql);
			stmt.setInt(1, objeto.getNumero());
			dados.remove(objeto);
			stmt.executeUpdate();
			stmt.close();
		} catch (SQLException ex) {
			throw new SQLException("Consultorio n�o foi removido" + ex);
		} finally {
			Conexao.getConnection().close();
		}
		
	}

	@Override
	public ArrayList<Consultorio> buscarHistorico() {
		// TODO Auto-generated method stub
		return dados;
	}



	


}
