package dao;

import java.sql.SQLException;
import java.util.ArrayList;

import exception.CpfInvalidoException;
import model.Funcionario;

public interface DaoGenerico <T> {
	
    /*public void cadastrar(T objeto) throws Exception;
    public ArrayList<T> buscar(T objeto) throws Exception;
    public ArrayList<T> buscar() throws Exception;
    public ArrayList<T> buscar(String coluna, String valor) throws Exception;
    public void alterar(T objeto) throws Exception;
    public void remover(T objeto) throws Exception;
    public ArrayList<T> buscarHistorico();
    
	//public void buscarIndi(T funcionario)throws ClassNotFoundException, SQLException, CpfInvalidoException;
	public T buscarInd(T objeto) throws Exception;*/
	
	
    public void cadastrar(T objeto) throws Exception;
    public T buscar(T objeto) throws Exception;
    public ArrayList<T> buscar() throws Exception;
    public ArrayList<T> buscar(String coluna, String valor) throws Exception;
    public void alterar(T objeto) throws Exception;
    public void remover(T objeto) throws Exception;
    public ArrayList<T> buscarHistorico();

}
