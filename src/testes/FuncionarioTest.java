package testes;

import org.junit.Test;

import exception.CpfInvalidoException;
import exception.SenhaInvalidaException;
import model.Funcionario;

public class FuncionarioTest {
	
	
	@Test(expected= CpfInvalidoException.class)
	public void testLoginCpfInvalidoExceptionCpfESenhaIncorretos() throws CpfInvalidoException, Exception{
		Funcionario f = new Funcionario();
		Funcionario func = new Funcionario("125365", "njdjmd");
		
		func = f.login(func.getCpf(),func.getSenha());	
		
	}
	
	
	
	@Test(expected = SenhaInvalidaException.class)
	public void testSenhaInvalidaExceptionCpfCorretoESenhaIncorreta () throws CpfInvalidoException, Exception {
		Funcionario f = new Funcionario();
		Funcionario func = new Funcionario ("010203", "896523");
		
		func = f.login(func.getCpf(), func.getSenha());
		
	}
	
	@Test(expected = CpfInvalidoException.class)
	public void testLoginInvalidoExceptionCpfIncorretoESenhaIncorreta() throws CpfInvalidoException, Exception{
		Funcionario f = new Funcionario();
		Funcionario func = new Funcionario ("0102030506", "18008657");
		
		func = f.login(func.getCpf(), func.getSenha());
	}
	
	
	
	

}
