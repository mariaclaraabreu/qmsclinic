package model;

public class Ficha {
	private int id;
	private boolean estado;
	
	
	public Ficha() {
		
	}
	
	public Ficha(int id, boolean estado) {
		this.id=id;
		this.estado=estado;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public boolean isEstado() {
		return estado;
	}

	public void setEstado(boolean estado) {
		this.estado = estado;
	}
	
	
	

}
