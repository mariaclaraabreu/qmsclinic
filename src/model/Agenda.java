package model;

import java.util.ArrayList;

public class Agenda {
	
	private int id;
	private String data;
	private String horario;
	private boolean status;
	
	//ver melhor isso aqui
	private final ArrayList <Medico> dados = new ArrayList<>();

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getData() {
		return data;
	}

	public void setData(String data) {
		this.data = data;
	}

	public String getHorario() {
		return horario;
	}

	public void setHorario(String horario) {
		this.horario = horario;
	}

	public boolean isStatus() {
		return status;
	}

	public void setStatus(boolean status) {
		this.status = status;
	}

	public ArrayList<Medico> getDados() {
		return dados;
	}
	
	
	///definir construtor para fazer trataments dos horarios disponiveis do medico
	
	
}
