package model;

import java.util.ArrayList;

import dao.ConsultaDao;
import dao.ConsultorioDao;
import dao.DaoGenerico;

public class Consultorio {
	private int numero;
	private int bloco;
	private Medico medico;
	private final DaoGenerico dao = new ConsultorioDao();
	
	
	/* Construtores*/
	
	public Consultorio() {
		
	}
	
	public Consultorio(int numero, int bloco, Medico medico) {
		this.numero=numero;
		this.bloco=bloco;
		this.medico=medico;
	}
	
	
	
	public Consultorio(int numero) {
		this.numero=numero;
	}

	public Consultorio(int numero, int bloco, String crmMedico) throws Exception {
	
		this.numero=numero;
		this.bloco=bloco;
		Medico med = new Medico(crmMedico);
		this.medico = med.buscar(med);
		
		//criar medico para buscar aqui
		
		
	}
	
	/*Getter e Setters*/

	public int getNumero() {
		return numero;
	}
	public void setNumero(int numero) {
		this.numero = numero;
	}
	public int getBloco() {
		return bloco;
	}
	public void setBloco(int bloco) {
		this.bloco = bloco;
	}
	public Medico getMedico() {
		return medico;
	}
	public void setMedico(Medico medico) {
		this.medico = medico;
	}
	
	/* Metodos crud */
	
	public void cadastrar(Consultorio objeto) throws Exception {
		dao.cadastrar(objeto);
	}

	public Consultorio buscar(Consultorio objeto) throws Exception {
		return (Consultorio) dao.buscar(objeto);
	}

	public ArrayList<Consultorio> buscar() throws Exception {
		return dao.buscar();
	}

	public ArrayList<Consultorio> buscar(String coluna, String valor) throws Exception {
		return dao.buscar(coluna, valor);
	}

	public void alterar(Consultorio objeto) throws Exception {
		dao.alterar(objeto);
	}

	public void remover(Consultorio objeto) throws Exception {
		dao.remover(objeto);
	}

	public ArrayList<Consultorio> buscarHistorico() {
		return dao.buscarHistorico();
	}
	
	/*Metodos regras de neg�cio*/
	
	
	
}
