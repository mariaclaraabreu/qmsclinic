package model;

import java.sql.Date;
import java.util.ArrayList;

import dao.DaoGenerico;
import dao.ConsultaDao;

/*Construtores*/

public final class Consulta {
	private int id;
	private Consultorio consultorio;
	private Medico medico;
	private Paciente paciente;
	private String horarioInicio;
	private String horarioTermino;
	private String dataConsulta;
	private boolean estado;

	private final DaoGenerico dao = new ConsultaDao();

	public Consulta() {

	}

	public Consulta(int id, Consultorio consultorio, Medico medico, Paciente paciente, String horarioInicio, String horarioTermino, String dataConsulta,
			boolean estado) {
		this.id = id;
		this.consultorio = consultorio;
		this.medico = medico;
		this.paciente = paciente;
		this.horarioInicio = horarioInicio;
		this.horarioTermino= horarioTermino;
		this.dataConsulta = dataConsulta;
		this.estado = estado;
	}

	public Consulta(int id, int numConsultorio, String crmMedico, String cpfPaciente, String horarioInicio, String horarioTermino, String dataConsulta, boolean estado) throws Exception {
		this.id=id;
		Consultorio cons = new Consultorio(numConsultorio);
		Medico med = new Medico(crmMedico);
		Paciente pac = new Paciente(cpfPaciente);
		this.consultorio= cons.buscar(cons);
		this.medico= med.buscar(med);
		this.paciente= pac.buscar(pac);
		this.horarioInicio=horarioInicio;
		this.horarioTermino=horarioTermino;
		this.dataConsulta=dataConsulta;
		this.estado=estado;
	}
		
		
		/*Getters e setters*/

	public int getId() {
		return id;
	}

	public String getHorarioInicio() {
		return horarioInicio;
	}

	public void setHorarioInicio(String horarioInicio) {
		this.horarioInicio = horarioInicio;
	}

	public String getHorarioTermino() {
		return horarioTermino;
	}

	public void setHorarioTermino(String horarioTermino) {
		this.horarioTermino = horarioTermino;
	}

	public DaoGenerico getDao() {
		return dao;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Consultorio getConsultorio() {
		return consultorio;
	}

	public void setConsultorio(Consultorio consultorio) {
		this.consultorio = consultorio;
	}

	public String getDataConsulta() {
		return dataConsulta;
	}

	public void setDataConsulta(String dataConsulta) {
		this.dataConsulta = dataConsulta;
	}

	public Medico getMedico() {
		return medico;
	}

	public void setMedico(Medico medico) {
		this.medico = medico;
	}

	public Paciente getPaciente() {
		return paciente;

	}

	public void setPaciente(Paciente paciente) {
		this.paciente = paciente;

	}

	public boolean isEstado() {
		return estado;
	}

	public void setStatus(boolean estado) {
		this.estado = estado;
	}

	/* Metodos CRUD*/

	public void cadastrar(Consulta objeto) throws Exception {
		dao.cadastrar(objeto);
	}

	public Consulta buscar(Consulta consulta) throws Exception {
		return (Consulta) dao.buscar(consulta);
	}

	public ArrayList<Consulta> buscar() throws Exception {
		return dao.buscar();
	}

	public ArrayList<Consulta> buscar(String coluna, String valor) throws Exception {
		return dao.buscar(coluna, valor);
	}

	public void alterar(Consulta consulta) throws Exception {
		dao.alterar(consulta);
	}

	public void remover(Consulta consulta) throws Exception {
		dao.remover(consulta);
	}

	public ArrayList<Consulta> buscarHistorico() {
		return dao.buscarHistorico();
	}
	
	/*Metodos regras de neg�cio*/
	
	
	

}
