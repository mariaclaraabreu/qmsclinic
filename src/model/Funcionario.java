package model;

import java.util.ArrayList;

import dao.DaoGenerico;

import dao.FuncionarioDao;
import exception.CpfInvalidoException;
import exception.SenhaInvalidaException;

public final class Funcionario {
	private String cpf;
	private String senha;
	
	private final DaoGenerico<Funcionario> dao = new FuncionarioDao();
	
	
	/* Construtores */
	
	public Funcionario() {
		
	}
	
	public Funcionario(String cpf, String senha) {
		this.cpf=cpf;
		this.senha=senha;
	}
	
	public Funcionario (String cpf) {
		this.cpf=cpf;
	}
	
	
	/*Getters e settters  */
	public String getCpf() {
		return cpf;
	}
	
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	
	public String getSenha() {
		return senha;
	}
	
	public void setSenha(String senha) {
		this.senha = senha;
	}
	

	
/*Metodos CRUD*/
	
	public void cadastrar(Funcionario objeto) throws Exception {
		dao.cadastrar(objeto);
	}

	public Funcionario buscar(Funcionario objeto) throws Exception {
		return (Funcionario) dao.buscar(objeto);
	}

	public ArrayList<Funcionario> buscar() throws Exception {
		return dao.buscar();
	}

	public ArrayList<Funcionario> buscar(String coluna, String valor) throws Exception {
		return dao.buscar(coluna, valor);
	}

	public void alterar(Funcionario objeto) throws Exception {
		dao.alterar(objeto);
	}

	public void remover(Funcionario objeto) throws Exception {
		dao.remover(objeto);
	}

	public ArrayList<Funcionario> buscarHistorico() {
		return dao.buscarHistorico();
	}


/*Metodos regras de neg�cio*/
	
	
	
    public Funcionario login(String cpf, String senha) throws CpfInvalidoException, Exception{
    	//FuncionarioDao fd = null;
        Funcionario func = buscar(new Funcionario(cpf));
        
       // System.out.println("Entrou aqui");
        
        //System.out.println(func+" "+cpf+" "+senha);
        if(func!=null){
        	verificarSenha(func,senha);
            return func;
        }else{
            
            throw new CpfInvalidoException("Usu�rio n�o encontrado");
            
        }
    }
    
    
    public void verificarSenha(Funcionario func, String senha) throws SenhaInvalidaException {
    	
    	if(!func.getSenha().equals(senha)){
           
             throw new SenhaInvalidaException("Senha incorreta");
         }
    }
    
    
    
    
    
    
    
	
	
}
