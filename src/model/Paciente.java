package model;

import java.util.ArrayList;

import dao.DaoGenerico;
import dao.PacienteDao;

public class Paciente {
	
	private String cpf;
	private String nome;
	private String endereco;
	private String telefone;
	private String email;
	
	private final DaoGenerico dao = new PacienteDao();
	
	
	
	/*Construtores*/
	
	public Paciente (String cpf, String nome, String endereco, String telefone, String email) {
		this.cpf=cpf;
		this.nome=nome;
		this.endereco=endereco;
		this.telefone=telefone;
		this.email=email;
	}
	
	public Paciente() {
		
	}
	
	public Paciente(String cpf) {
		this.cpf=cpf;
	}
	
	
	
	/*Getters e Setters*/

	public String getCpf() {
		return cpf;
	}
	public void setCpf(String cpf) {
		this.cpf = cpf;
	}
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEndereco() {
		return endereco;
	}
	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}
	public String getTelefone() {
		return telefone;
	}
	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	
	/* Metodos CRUD*/

	public void cadastrar(Paciente objeto) throws Exception {
		dao.cadastrar(objeto);
	}

	public Paciente buscar(Paciente objeto) throws Exception {
		return (Paciente) dao.buscar(objeto);
	}

	public ArrayList<Paciente> buscar() throws Exception {
		return dao.buscar();
	}

	public ArrayList<Paciente> buscar(String coluna, String valor) throws Exception {
		return dao.buscar(coluna, valor);
	}

	public void alterar(Paciente objeto) throws Exception {
		dao.alterar(objeto);
	}

	public void remover(Paciente objeto) throws Exception {
		dao.remover(objeto);
	}

	public ArrayList<Paciente> buscarHistorico() {
		return dao.buscarHistorico();
	}
	
	/*Metodos regras de neg�cio*/
	
	

	
	
	
	
}
