package model;

import java.util.ArrayList;

import dao.DaoGenerico;
import dao.MedicoDao;
import exception.CpfInvalidoException;
import exception.SenhaInvalidaException;

public class Medico {
	
	private String crm;
	private String senha;
	private String nome;
	private String especialidade;
	
	ArrayList<Consulta> agendaMedico;
	
	private final DaoGenerico dao = new MedicoDao();

	

	
	/* Construtores*/
	
	public Medico( String crm, String senha, String nome, String especialidade, ArrayList agendaMedico) {
		this.crm = crm;
		this.senha=senha;
		this.nome=nome;
		this.especialidade= especialidade;
		this.agendaMedico = agendaMedico;

	}
	
	
	public Medico( String crm, String senha, String nome, String especialidade) {
		this.crm = crm;
		this.senha=senha;
		this.nome=nome;
		this.especialidade= especialidade;

	
	}
	
	
	public Medico() {
		
	}
	public Medico(String crm) {
		this.crm=crm;
	}
	
	
	/* Getters e Setters*/
	
	
	
	public Agenda getAgenda() {
		return getAgenda();
	}
	
	public ArrayList<Consulta> getAgendaMedico() {
		return agendaMedico;
	}

	public void setAgendaMedico(ArrayList<Consulta> agendaMedico) {
		this.agendaMedico = agendaMedico;
	}

	
	public String getSenha() {
		return senha;
	}
	public void setSenha(String senha) {
		this.senha = senha;
	}
	public String getCrm() {
		return crm;
	}
	public void setCrm(String crm) {
		this.crm = crm;
	}
	
	public String getNome() {
		return nome;
	}
	public void setNome(String nome) {
		this.nome = nome;
	}
	public String getEspecialidade() {
		return especialidade;
	}
	public void setEspecialidade(String especialidade) {
		this.especialidade = especialidade;
	}
	
	/*Metodos CRUD*/
	
	
	public void cadastrar(Medico objeto) throws Exception {
		dao.cadastrar(objeto);
	}

	public Medico buscar(Medico objeto) throws Exception {
		return (Medico) dao.buscar(objeto);
	}

	public ArrayList<Medico> buscar() throws Exception {
		return dao.buscar();
	}

	public ArrayList<Medico> buscar(String coluna, String valor) throws Exception {
		return dao.buscar(coluna, valor);
	}

	public void alterar(Medico objeto) throws Exception {
		dao.alterar(objeto);
	}

	public void remover(Medico objeto) throws Exception {
		dao.remover(objeto);
	}

	public ArrayList<Medico> buscarHistorico() {
		return dao.buscarHistorico();
	}
	
	/*Metodos regras de neg�cio*/
	
	public void verificarAgendaMedico() {
		
	}
	
	
	  public Medico login(String cpf, String senha) throws CpfInvalidoException, Exception{
	    	//FuncionarioDao fd = null;
	        Medico med = buscar(new Medico(crm));
	        
	       // System.out.println("Entrou aqui");
	        
	        //System.out.println(func+" "+cpf+" "+senha);
	        if(med!=null){
	        	verificarSenha(med,senha);
	            return med;
	        }else{
	            
	            throw new CpfInvalidoException("Usu�rio n�o encontrado");
	            
	        }
	    }
	    
	    
	    public void verificarSenha(Medico med, String senha) throws SenhaInvalidaException {
	    	
	    	if(!med.getSenha().equals(senha)){
	           
	             throw new SenhaInvalidaException("Senha incorreta");
	         }
	    }
	    
	    
	    
	
	
	
	

	
	
	
	
}
